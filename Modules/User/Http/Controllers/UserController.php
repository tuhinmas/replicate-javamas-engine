<?php

namespace Modules\User\Http\Controllers;

use App\Models\User;
use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Role;
use Modules\Profile\Entities\Profile;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Illuminate\Contracts\Support\Renderable;

class UserController extends Controller
{
    use HasRoles;
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        //get all profile users with their roles
        $users = Profile::with('position', 'users')->get();
        // dd($users);

        return view('user::index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        //get all role
        $roles = Role::all();

        //role default id
        $role_default_id = Role::findByName('default');
        $position = Position::all();
        $position_default_id = Position::where('position', 'staff')->first();

        //pass role collections to create view
        return view('user::create', compact('roles', 'role_default_id', 'position', 'position_default_id'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //validate input form
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
            'hoby' => 'required',
            'address' => 'required',
            'hp' => 'required',
        ]);

        //create user from request
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        //create profile for user
        Profile::create([
            'name' => $request->name,
            'address' => $request->address,
            'hp' => $request->hp,
            'hoby' => $request->hoby,
            'user_id' => $user->id,
            'position_id' => $request->position,
        ]);

        //assign role for user was created
        $user->assignRole($request->role);

        return redirect()->route('users.list');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $profile = Profile::with('position', 'users')
            ->where('id', $id)
            ->first();
            // dd($profile);
        $position_roles = $this->position_role();
        return view('user::edit', compact('profile', 'position_roles'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $profile = Profile::find($id);
        $user = User::find($id);
        // dd($profile);

        //validate input form
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'. $request->user_id,
            'hoby' => 'required',
            'address' => 'required',
            'hp' => 'required',
        ]);

        // dd($request->all());

        //update user accoutn
            $profile->name = $request->name;
            $profile->hoby = $request->hoby;
            $profile->address = $request->address;
            $profile->hp = $request->hp;
            $profile->position_id = $request->position;
            $profile->user_id = $request->user_id;
            $profile->save();

            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();

            $user->syncRoles($request->role);
        return redirect()->route('users.list');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('users.list');
    }

    public function position_role()
    {
        $position = Position::all();
        $roles = Role::all();
        $position_roles = (object) [
            'position' => $position,
            'roles' => $roles,
        ];
        return $position_roles;
    }

    public function edit_permission($id){
        //get user by id
        $user = User::with('roles','profile')->where('id',$id)->first();

        //get user's role and permission
        $roles = $user->getAllPermissions();
        $permissions = Permission::all();
        // dd($role);

        //pass data to view
        return view('user::editPermission', compact('user','permissions','roles'));
    }

    /**
     * update permission
     *
     * @param Request $request
     * $request->permission = string
     * @param [type] $id
     * @return void
     */
    public function update_permission(Request $request, $id){
        $user = User::find($id);

        //assaign permission to user directly
        $user->givePermissionTo($request->permission);

        // dd($user->getAllPermissions());
        return redirect()->route('users.list');
    }
}
