<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'read article']);
        Permission::create(['name' => 'add new article']);
        Permission::create(['name' => 'edit article']);
        Permission::create(['name' => 'delete article']);

        //create default permisssion
        Permission::create([
            'name' => 'show profile'
        ]);

        // create roles and assign created permissions

        // or may be done by chaining

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(['read article', 'add new article', 'edit article', 'delete article']);

        //create default role
        $role2 = Role::create([
            'name' => 'default'
        ]);

        //assign default permission to default role
        $role2->givePermissionTo(['show profile','read article']);
    }
}
