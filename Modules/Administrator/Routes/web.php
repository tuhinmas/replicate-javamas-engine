<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    "prefix" => "administrator",
    "middleware" => ['auth']
    ], function(){
        Route::get('/', 'AdministratorController@index')->name('admin.users');
        Route::get('/create', 'AdministratorController@create')->name('admin.users.add');
        Route::post('/store', 'AdministratorController@store')->name('admin.users.store');
        Route::get('/show/{id}', 'AdministratorController@show')->name('admin.users.show');        
        Route::get('/delete/{id}', 'AdministratorController@destroy')->name('admin.users.delete');        
        Route::post('/update/{id}', 'AdministratorController@update')->name('admin.users.update');        
        Route::get('/assign', 'AdministratorController@assign')->name('admin.users.assign');        
        Route::post('/assignPermission', 'AdministratorController@assignPermission')->name('admin.users.assignPermission');        
});
