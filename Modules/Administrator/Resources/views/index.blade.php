@extends('coreui::master')

@push('css')

@endpush

@section('title', 'Dashboard')

@section('breadcrumb')
    <li class="breadcrumb-item">a breadcrumb item</li>
@stop

@section('body')
    <h1>Admin Dashboard</h1>
    <div class="card">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Role Name</th>
                    <th>Permission</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users->roles as $role)
                    @foreach ($role->permissions as $key => $permission)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $role->name }}</td>
                            <td>{{ $permission->name }}</td>
                            <td>
                                @can('tambah akun baru')
                                    <a class=" mr-3" href="{{ route('admin.users.show', ['id' => $permission->id]) }}">
                                        <i class="fas fa-edit fa-2x" style="color: blue"></i>
                                    </a>
                                @endcan
                                @can('delete administrator permission')
                                    <a class="ml-3"
                                        href="{{ route('admin.users.delete', ['id' => $permission->id]) }}">
                                        <i class="fas fa-trash-alt fa-2x" style="color: black"></i>
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('footer')
    <p>Javamas 2021</p>
@endsection

@push('js')

@endpush
