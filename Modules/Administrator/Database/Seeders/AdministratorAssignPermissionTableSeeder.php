<?php

namespace Modules\Administrator\Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AdministratorAssignPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //assign role super-admin to user with name admin
        $user = User::where('name','admin')->first();
        $user->assignRole('super-admin');
    }
}
