<?php

namespace Modules\Administrator\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdministratorPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        //create permidssion
        Permission::create(['name' => 'edit administrator permission']);
        Permission::create(['name' => 'delete administrator permission']);
        Permission::create(['name' => 'create administrator permission']);
        Permission::create(['name' => 'assign administrator permission']);
        Permission::create(['name' => 'show administrator permission']);

        //create roles and assign that to created permission
        //or may be done by chaining

        $role =Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());
    }
}
