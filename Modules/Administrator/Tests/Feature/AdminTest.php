<?php

namespace Modules\Administrator\Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Tests\TestCase;

class AdminTest extends TestCase
{
    use HasRoles;
    use RefreshDatabase;
    use WithoutMiddleware;
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * admin form create new permission.
     *
     * @test
     */
    public function admin_can_see_form_create_user()
    {
        $response = $this->actingAs($this->user())->get('administrator/create');
        $response->assertStatus(200);
    }

    /**
     *admin store new permission
     *
     * @test
     */
    public function admin_create_new_permission()
    {
        $response = $this->actingAs($this->user())->post('/administrator/store',
            [
                'name' => 'create article',
            ]);
        $response->assertRedirect('/administrator');
    }

    /**
     * show specified permission by id
     *
     * @test
     */
    public function admin_show_specified_user_permission()
    {
        $permission = Permission::create([
            'name' => 'aditiya',
        ]);
        $response = $this->actingAs($this->user())->get(route("admin.users.show", ['id' => $permission->id]));
        $response->assertSeeText(['permission', 'role']);
    }

    /**
     * admin can update permission name
     *
     * @test
     */
    public function admin_can_update_permission_name()
    {
        $permission = Permission::create([
            'name' => 'create user',
        ]);

        $response = $this->actingAs($this->user())
            ->post("/administrator/update/$permission->id",
                [
                    'name' => 'create new user',
                ]);
        $response->assertRedirect(route('admin.users'));
    }

    /**
     * check if form update is empty
     */
    public function if_form_update_empty()
    {
        $permission = Permission::create([
            'name' => 'create user',
        ]);

        $response = $this->actingAs($this->user())
            ->post("/administrator/update/$permission->id",
                [
                    'name' => '',
                ]);
        $response->assertSessionHasErrors('name');
    }

    /**
     * admin revoke permission from user
     *
     * @test
     */
    public function admin_can_revoke_permission_from_user()
    {
        $response = $this->actingAs($this->user())
            ->get(route('admin.users.delete', [
                'id' => $this->create_permission()->permission->id,
            ]));
        $response->assertRedirect(route('admin.users'));
    }

    /**
     * form assign permisssion to user
     * @test
     */
    public function assign_permission()
    {
        $response = $this->actingAs($this->user())
            ->get(route("admin.users.assign"));

        $response->assertStatus(200);
    }

    /**
     * assign permiddion to user
     * @test
     */
    public function assign_permission_to_user()
    {
        $permission = $this->create_permission();
        // dd($permission->permission);
        $response = $this->actingAs($permission->user)
            ->post(route("admin.users.assignPermission",[
                'permission' => $permission->permission->id,
            ]));
        $response->assertRedirect(route('admin.users'));
    }
    /**
     * inisial permission
     *
     * @return void
     */
    public function create_permission()
    {
        $user = User::factory()->create();
        $permission = Permission::create(['name' => 'create article']);
        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::All());
        $user->assignRole('admin');

        $data = (object) [
            'permission' => $permission,
            'role' => $role,
            'user' => $user,
        ];
        return $data;
    }

    /**
     * user initial set up for testing
     *
     * @return void
     */
    public function user()
    {
        $user = User::factory()->create();
        $role = Role::create(['name' => 'super-admin']);
        $permission = Permission::create(['name' => 'show administrator permission']);
        $role->givePermissionTo(Permission::all());
        $user->assignRole('super-admin');

        return $user;
    }
}
