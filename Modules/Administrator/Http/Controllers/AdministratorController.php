<?php

namespace Modules\Administrator\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Illuminate\Contracts\Support\Renderable;

class AdministratorController extends Controller
{
    use HasRoles;
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $users = User::role('admin')->first();
        $permissions = $users->getAllPermissions();

        // return response()->json($users);

        return view('administrator::index', compact(['users']));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('administrator::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|min:3|max:50',
        ]);

        $permission = Permission::create([
            'name' => $request->name,
        ]);

        return redirect()->route('admin.users');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $permission = Permission::find($id);
        $role = $this->roleAdmin();;

        return view('administrator::show', compact('permission','role'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        
        return view('administrator::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:50',
        ]);

        $permission = Permission::find($id);
        $permission->name = $request->name;
        $permission->save();

        return redirect()->route('admin.users');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $permission = Permission::find($id);
        $this->roleAdmin()->revokePermissionTo($permission);

        return redirect()->route('admin.users');
    }

    /**
     * get permission order by id descending
     *
     * @return void
     */
    public function assign(){
        $permissions = Permission::orderBy('id','desc')->get();

        return view('administrator::assign',compact('permissions'));
    }

    /**
     * Assign permission to admin
     *
     * @param Request $request->permision =integer id
     * @return void
     */
    public function assignPermission(Request $request){
        $permission = Permission::find($request->permission);
        $permission->assignRole($this->roleAdmin());

        return redirect()->route('admin.users');
    }

    /**
     * get role by name
     *
     * @return void
     */
    public function roleAdmin(){
        $role = Role::findByName('admin');
        return $role;
    }
}
