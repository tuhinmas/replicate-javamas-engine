<?php

namespace Modules\Supervisor\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class SupervisorPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'tambah akun baru']);
        Permission::create(['name' => 'delete akun']);

        // create roles and assign created permissions
        // this can be done as separate statements
        $role = Role::create(['name' => 'supervisor']);
        $role->givePermissionTo('tambah akun baru');
    }
}
