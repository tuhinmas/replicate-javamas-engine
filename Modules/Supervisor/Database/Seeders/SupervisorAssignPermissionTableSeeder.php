<?php

namespace Modules\Supervisor\Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SupervisorAssignPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //assign role super-admin to user with name admin
        $user = User::where('name','supervisor')->first();
        $user->assignRole('supervisor');
    }
}
