@extends('coreui::master')

@push('css')

@endpush

@section('title', 'Profile')

@section('breadcrumb')
    <li class="breadcrumb-item">a breadcrumb item</li>
@stop

@section('body')
    <h1>Profile</h1>
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Profile User
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <p>Javamas 2021</p>
@endsection

@push('js')

@endpush
