@extends('coreui::master')

@push('css')

@endpush

@section('title', 'Profile')

@section('breadcrumb')
    <li class="breadcrumb-item">a breadcrumb item</li>
@stop

@section('body')
    <h1>Profile</h1>
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Data Pegawai
            </div>
        </div>
        <div class="card-body">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <strong>Nama</strong>
                        </td>
                        <td>
                            <div>
                                : {{ $profile->name }}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Alamat</strong>
                        </td>
                        <td>
                            : {{ $profile->address }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>No HP</strong>
                        </td>
                        <td>
                            : {{ $profile->hp }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Hobi : </strong>
                        </td>
                        <td>
                            : {{ $profile->hoby }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Jabatan</strong>
                        </td>
                        <td>
                            : {{ $profile->position->position }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    <p>Javamas 2021</p>
@endsection

@push('js')

@endpush
