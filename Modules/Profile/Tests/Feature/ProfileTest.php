<?php

namespace Modules\Profile\Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Position;
use Modules\Profile\Entities\Profile;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * unauthentucated user redirect to login
     */
    public function only_authenicated_user_can_see_profil()
    {
        $respone = $this->get('/profile');
        $respone->assertRedirect('login');
    }

    /**
     * logged in users can see his profiles
     * @test
     */
    public function user_logged_in_can_see_his_profile()
    {
        // Role::create(['name' => 'admin']);
        $position = Position::create([
            'position' => 'manager',
            'salary' => '8',
        ]);

        $user = User::factory()->create();
        $profile = Profile::create([
            '_token' => csrf_token(),
            'name' => 'Hari Sungkawa',
            'hoby' => 'kerja',
            'address' => 'mbuh_adoh',
            'hp' => '0851231231',
            'user_id' => $user->id,
            'position_id' => $position->id,
        ]);

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);
        $this->assertAuthenticated();
        $response = $this->get('/profile');
        // dd($response);
        $response->assertStatus(200);
    }
}
