<?php

namespace Modules\Profile\Database\Seeders;

use Illuminate\Database\Seeder;
use \Modules\Profile\Entities\Profile;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create array users detail
        $profiles = [
            [
                "name" => 'Adi Ms',
                'address' => 'Jl. Kaliurang no 10',
                'hp' => '085123123123',
                'hoby' => 'Turonan',
                'user_id' => '1',
                'position_id' => '1',
            ],
            [
                "name" => 'Bagusajiwo',
                'address' => 'Jl. Adisucipto no 10',
                'hp' => '085123123123',
                'hoby' => 'Turonan',
                'user_id' => '2',
                'position_id' => '2',
            ],
            [
                "name" => 'Steven',
                'address' => 'Jl. Ahmad Dahlan no 30',
                'hp' => '085123123123',
                'hoby' => 'Turonan',
                'user_id' => '3',
                'position_id' => '3',
            ],
        ];

        //assign array to database with loop
        foreach ($profiles as $profile) {
            Profile::create($profile);
        }
    }
}
