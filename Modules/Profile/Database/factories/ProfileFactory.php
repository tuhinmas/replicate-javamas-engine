<?php
namespace Modules\Profile\Database\factories;

use App\Models\Position;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Profile\Entities\Profile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $position = Position::create([
            'position' => 'manager',
            'salary' => '8'
        ]);

        $user = User::factory()->create();
        return [
                '_token' => csrf_token(),
                'name' => 'Hari Sungkawa',
                'email' => 'haris@gmail.com',
                'hoby' => 'kerja',
                'address' => 'mbuh_adoh',
                'hp' => '0851231231',
                'user_id' => $user->id,
                'position_id' => $position->id,
        ];
    }
}

