<?php

namespace Modules\Profile\Entities;

use App\Models\User;
use App\Models\Position;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Profile extends Model
{
    use HasFactory;
    use HasRoles;

    protected $guarded = [];
    protected $table = 'profiles';

    public function users(){
        return $this->hasOne(User::class,'id','user_id')->with('roles');
        // return $user->roles();
    }

    public function position(){
        return $this->hasOne(Position::class,'id','position_id');
    }
}