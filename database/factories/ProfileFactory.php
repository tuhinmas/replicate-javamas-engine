<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Model;
use App\Models\Profile;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Profile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = User::count();
            return [
                'name' => $this->faker->name,
                'address' => $this->faker->address,
                'hp' => $this->faker->phoneNumber,
                'hoby' => 'turonan',
                'user_id' => $this->faker->unique(true)->numberBetween(1,3),
                'position_id' => $this->faker->unique(true)->numberBetween(1,3),
            ];
    }
}
