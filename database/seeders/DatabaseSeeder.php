<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\PositionTableSeeder;
use Modules\Profile\Database\Seeders\ProfileTableSeeder;
use Modules\User\Database\Seeders\UserPermissionTableSeeder;
use Modules\User\Database\Seeders\UserAssignPermissionTableSeeder;
use Modules\Supervisor\Database\Seeders\SupervisorPermissionTableSeeder;
use Modules\Administrator\Database\Seeders\AdministratorPermissionTableSeeder;
use Modules\Supervisor\Database\Seeders\SupervisorAssignPermissionTableSeeder;
use Modules\Administrator\Database\Seeders\AdministratorAssignPermissionTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::Class);
        $this->call(PositionTableSeeder::Class);
        $this->call(ProfileTableSeeder::Class);
        $this->call(UserPermissionTableSeeder::Class);
        $this->call(UserAssignPermissionTableSeeder::Class);
        $this->call(SupervisorPermissionTableSeeder::Class);
        $this->call(SupervisorAssignPermissionTableSeeder::Class);
        $this->call(AdministratorPermissionTableSeeder::Class);
        $this->call(AdministratorAssignPermissionTableSeeder::Class);
    }
}
