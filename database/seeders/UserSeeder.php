<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'admin',
                'email' => 'admin@mail.com',
                'password' => bcrypt('password'),
            ],
            [
                'name' => 'user',
                'email' => 'user@mail.com',
                'password' => bcrypt('secret'),
            ],
            [
                'name' => 'supervisor',
                'email' => 'supervisor@mail.com',
                'password' => bcrypt('secret'),
            ],

        ];
        foreach($users as $user){
            User::create($user);
        }
    }
}
