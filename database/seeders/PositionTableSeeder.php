<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Seeder;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positions = [
            [
                'position' => 'Manajer',
                'salary' => '8',
            ],
            [
                'position' => 'Supervisor',
                'salary' => '7',
            ],
            [
                'position' => 'Staff',
                'salary' => '6',
            ],
        ];
        foreach ($positions as $position) {
            Position::create($position);
        }
    }
}
